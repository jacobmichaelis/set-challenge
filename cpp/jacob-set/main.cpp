#include <iostream>
#include "nlohmann/json.hpp"

using namespace std;

int main(int argc, char *argv[]) {
	if (argc < 2) {
		cout << "no args" << endl;
		return 0;
	}

	// '[{"id":1,"number":2,"shape":1,"color":1,"fill":1},{"id":2,"number":1,"shape":3,"color":3,"fill":3},{"id":3,"number":3,"shape":2,"color":1,"fill":3},{"id":4,"number":3,"shape":3,"color":2,"fill":3},{"id":5,"number":1,"shape":2,"color":1,"fill":2},{"id":6,"number":1,"shape":1,"color":3,"fill":2},{"id":7,"number":1,"shape":1,"color":1,"fill":1},{"id":8,"number":1,"shape":1,"color":3,"fill":1},{"id":9,"number":2,"shape":1,"color":2,"fill":3},{"id":10,"number":1,"shape":1,"color":3,"fill":3},{"id":11,"number":3,"shape":1,"color":1,"fill":1},{"id":12,"number":1,"shape":2,"color":3,"fill":2}]'
	// string test = "[{\"id\":1,\"number\":2,\"shape\":1,\"color\":1,\"fill\":1},{\"id\":2,\"number\":1,\"shape\":3,\"color\":3,\"fill\":3},{\"id\":3,\"number\":3,\"shape\":2,\"color\":1,\"fill\":3},{\"id\":4,\"number\":3,\"shape\":3,\"color\":2,\"fill\":3},{\"id\":5,\"number\":1,\"shape\":2,\"color\":1,\"fill\":2},{\"id\":6,\"number\":1,\"shape\":1,\"color\":3,\"fill\":2},{\"id\":7,\"number\":1,\"shape\":1,\"color\":1,\"fill\":1},{\"id\":8,\"number\":1,\"shape\":1,\"color\":3,\"fill\":1},{\"id\":9,\"number\":2,\"shape\":1,\"color\":2,\"fill\":3},{\"id\":10,\"number\":1,\"shape\":1,\"color\":3,\"fill\":3},{\"id\":11,\"number\":3,\"shape\":1,\"color\":1,\"fill\":1},{\"id\":12,\"number\":1,\"shape\":2,\"color\":3,\"fill\":2}]";

	std::string input = argv[1];

  auto cards = nlohmann::json::parse(input);

	if (sizeof(cards) < 3) {
		cout << "need more cards" << endl;
		return 0;
	}

  for (auto card1 : cards) {
    for (auto card2 : cards) {
      for (auto card3 : cards) {
				if (card1 == card2 || card2 == card3 || card3 == card1) {
					continue;
				}

        int numberSum = (int)card1["number"] + (int)card2["number"] + (int)card3["number"];
        int shapeSum = (int)card1["shape"] + (int)card2["shape"] + (int)card3["shape"];
        int colorSum = (int)card1["color"] + (int)card2["color"] + (int)card3["color"];
        int fillSum = (int)card1["fill"] + (int)card2["fill"] + (int)card3["fill"];

				if ((numberSum % 3) == 0 && (shapeSum % 3) ==0 && (colorSum % 3) == 0 && (fillSum % 3) == 0) {
					cout << card1["id"].dump() << "," << card2["id"].dump() << "," << card3["id"].dump() << endl;
					return 0;
				}
			}
		}
  }

  cout << "no set" << endl;
  return 0;
}