package jacobset

import (
	"strconv"
	"jacob.set/card"
)

func FindSet(cardSet []card.Card) string {

	for i1, card1 := range cardSet {
		for i2, card2 := range cardSet {
			for i3, card3 := range cardSet {
				if i1 == i2 || i2 == i3 || i3 == i1 {
					continue
				}

				var checkSet []card.Card = []card.Card{card1, card2, card3}

				if ValidateSet(checkSet) {
					id0 := strconv.Itoa(checkSet[0].Id)
					id1 := strconv.Itoa(checkSet[1].Id)
					id2 := strconv.Itoa(checkSet[2].Id)
					return id0 + "," + id1 + "," + id2
				}
			}
		}
	}
	return "{\"error\":\"No set\"}"
}

func ValidateSet(cardSet []card.Card) bool {
	if len(cardSet) != 3 {
		return false
	}

	numberSum := cardSet[0].Number + cardSet[1].Number + cardSet[2].Number
	shapeSum := cardSet[0].Shape + cardSet[1].Shape + cardSet[2].Shape
	colorSum := cardSet[0].Color + cardSet[1].Color + cardSet[2].Color
	fillSum := cardSet[0].Fill + cardSet[1].Fill + cardSet[2].Fill

	return (numberSum % 3) == 0 && (shapeSum % 3) == 0 && (colorSum % 3) == 0 && (fillSum % 3) == 0
}
