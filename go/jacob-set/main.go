package main

import (
	"encoding/json"
	"fmt"
	"os"

	"jacob.set/card"
	"jacob.set/jacobset"
)

func main() {

	if (len(os.Args) < 2) {
		fmt.Println("{\"error\":\"No arguments\"}")
		return
	}

	input := os.Args[1]

	var cards []card.Card
	err := json.Unmarshal([]byte(input), &cards)

	if err != nil {
		fmt.Println("{\"error\":\"", err, "\"}")
		return
	}

	if len(cards) < 3 {
		fmt.Println("{\"error\":\"Not enough cards\"}")
		return
	}

	set := jacobset.FindSet(cards)

	fmt.Println(set)
	return
}
