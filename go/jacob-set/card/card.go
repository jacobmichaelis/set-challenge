package card

type Card struct {
	Id     int `json:"id"`
	Number int `json:"number"`
	Shape  int `json:"shape"`
	Color  int `json:"color"`
	Fill   int `json:"fill"`
}